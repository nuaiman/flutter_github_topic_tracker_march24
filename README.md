# flutter_github_topic_tracker_march24

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


// Read under this line -----

Commit-1 = Default flutter app. /n 

Commit-2 = Added this read me. /n 

Commit-3 = Added flutter_riverpod and http to pubspec.yaml. /n 

Commit-4 = Added initial folder structre. /n 

Commit-5 = From the assesment PDF, requirement-1 included a list of links for github. from it I took "https://api.github.com/search/repositories?q={query}{&page,per_page,sort,order}" and midified it to use in this application. My nd point can be found in /lib/apis/topic_api.dart. My endpoint - "https://api.github.com/search/repositories?q=flutter&page=$page&per_page=10". Before implementing, I have tested my endpoint from VScode's thunderClient. /n 

Commit-6 = Added a screen where api data can be called. Modified the main.dart. Also, added a controller to hold business logic of getting the topics. And one main screen where topics will  be shown. /n 

Commit-7 = Added code to topic screen to show fetched repos. Refactored the main list to a seprate widget and added bottom circular progress indicator. At this point I noticed on page 6 I get a null value from servr hence I couldnot ftch new iteams. So, i made the model null tolerant and also edited the api file. /n 

Commit-8 = Extracted large widgets into smaller chunks. /n 

Commit-9 = Sort by stars and updatedAt added. /n 

Commit-9 = Added topic details screen with required data. /n 

Commit-10 = Added db file to persist topics and images. /n 
  
Commit-11 = Changed the app's UI to be more aestheticly pleasing and added a few quality of life improvement --> scaffoldMessenger and etc. /n   

Commit-12 = Added 2 flavors, dev & prod. Prod is empty for now. All codes do reside inside dev. I just want to demonstrate how I setup flavors. launch.json code will be attached to assessment mail reply. /n   