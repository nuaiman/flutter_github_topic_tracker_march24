class Topic {
  final int id;
  final String name;
  final String description;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String ownerName;
  final String ownerImageUrl;
  final int stargazersCount;

  Topic({
    required this.id,
    required this.name,
    required this.description,
    required this.createdAt,
    required this.updatedAt,
    required this.ownerName,
    required this.ownerImageUrl,
    required this.stargazersCount,
  });

  factory Topic.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      throw ArgumentError('json must not be null');
    }
    return Topic(
      id: json['id'] as int,
      name: json['name'] as String? ?? '',
      description: json['description'] as String? ?? '',
      createdAt: DateTime.parse(json['created_at'] as String? ?? ''),
      updatedAt: DateTime.parse(json['updated_at'] as String? ?? ''),
      ownerName: json['owner']?['login'] as String? ?? '',
      ownerImageUrl: json['owner']?['avatar_url'] as String? ?? '',
      stargazersCount: json['stargazers_count'] as int? ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'created_at': createdAt.toIso8601String(),
      'updated_at': updatedAt.toIso8601String(),
      'owner': {
        'login': ownerName,
        'avatar_url': ownerImageUrl,
      },
      'stargazers_count': stargazersCount,
    };
  }

  Topic copyWith({
    int? id,
    String? name,
    String? description,
    DateTime? createdAt,
    DateTime? updatedAt,
    String? ownerName,
    String? ownerImageUrl,
    int? stargazersCount,
  }) {
    return Topic(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      ownerName: ownerName ?? this.ownerName,
      ownerImageUrl: ownerImageUrl ?? this.ownerImageUrl,
      stargazersCount: stargazersCount ?? this.stargazersCount,
    );
  }

  @override
  String toString() {
    return 'Topic(id: $id, name: $name, description: $description, createdAt: $createdAt, updatedAt: $updatedAt, ownerName: $ownerName, ownerImageUrl: $ownerImageUrl, stargazersCount: $stargazersCount)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Topic &&
        other.id == id &&
        other.name == name &&
        other.description == description &&
        other.createdAt == createdAt &&
        other.updatedAt == updatedAt &&
        other.ownerName == ownerName &&
        other.ownerImageUrl == ownerImageUrl &&
        other.stargazersCount == stargazersCount;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        description.hashCode ^
        createdAt.hashCode ^
        updatedAt.hashCode ^
        ownerName.hashCode ^
        ownerImageUrl.hashCode ^
        stargazersCount.hashCode;
  }
}
