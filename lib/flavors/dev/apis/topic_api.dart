import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../../models/topic.dart';

abstract class ITopicApi {
  Future<List<Topic>> fetchTopics({int page});
}

// -----------------------------------------------------------------------------
class TopicApi implements ITopicApi {
  @override
  Future<List<Topic>> fetchTopics({int page = 1}) async {
    final response = await http.get(
      Uri.parse(
          'https://api.github.com/search/repositories?q=flutter&page=$page&per_page=10'),
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> jsonResponse = json.decode(response.body);
      final List<dynamic> itemList = jsonResponse['items'];
      List<Topic> topics = [];
      for (var jsonTopic in itemList) {
        topics.add(Topic.fromJson(jsonTopic));
      }
      return topics;
    } else {
      final errorMessage = json.decode(response.body)['message'];
      throw Exception('Failed to load topics: $errorMessage');
    }
  }
}
// -----------------------------------------------------------------------------

final topicApiProvider = Provider<TopicApi>((ref) {
  return TopicApi();
});
