import 'package:flutter/material.dart';

import 'features/topic/screens/initialization.dart';

class GithubTopicsApp extends StatelessWidget {
  const GithubTopicsApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Github Topic Tracker',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
        scaffoldBackgroundColor: Color(0xFF08080B),
        appBarTheme: AppBarTheme(
            color: Color(0xFF08080B),
            surfaceTintColor: Color(0xFF08080B),
            foregroundColor: Colors.white),
      ),
      home: const InitializationScreen(),
    );
  }
}
