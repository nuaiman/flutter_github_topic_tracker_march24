import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../utils/utils.dart';

class TopicsSortController extends StateNotifier<bool> {
  TopicsSortController() : super(true);

  void changeFilter(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    state = !state;
    await prefs.setBool('filter', state);
    if (context.mounted) {
      showSnackbar(context, state == true ? '🌟' : '📅');
    }
  }

  Future<void> getFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    state = prefs.getBool('filter') ?? false;
  }
}

// -----------------------------------------------------------------------------

final topicSortProvider =
    StateNotifierProvider<TopicsSortController, bool>((ref) {
  return TopicsSortController();
});
