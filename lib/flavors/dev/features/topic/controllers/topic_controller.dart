import 'dart:async';

import 'package:flutter_github_topic_tracker_march24/flavors/dev/db/db.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../apis/topic_api.dart';
import '../../../../../models/topic.dart';

class TopicsController extends StateNotifier<List<Topic>> {
  final TopicApi _topicsApi;
  int _currentPage = 1;

  TopicsController({required TopicApi topicsApi})
      : _topicsApi = topicsApi,
        super([]);

  Future<void> fetchNextPage() async {
    List<Topic> newTopics = await _topicsApi.fetchTopics(page: _currentPage);
    if (newTopics.isNotEmpty) {
      _currentPage++;
      state = [...state, ...newTopics];
      await TopicSharedPreferences().saveTopics(state);
    }
  }

  Future<void> loadFromSharedPreferences() async {
    final topics = await TopicSharedPreferences().getTopics();
    state = topics;
  }
}

// -----------------------------------------------------------------------------

final topicsProvider =
    StateNotifierProvider<TopicsController, List<Topic>>((ref) {
  final topicsApi = ref.read(topicApiProvider);
  return TopicsController(
    topicsApi: topicsApi,
  );
});
