import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/topic_controller.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/topic_sort_controller.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../../models/topic.dart';

class FilteredTopicsController extends StateNotifier<List<Topic>> {
  final List<Topic> _topics;
  final bool _filter;
  FilteredTopicsController({
    required List<Topic> topics,
    required bool filter,
  })  : _topics = topics,
        _filter = filter,
        super([]);

  List<Topic> getFilteredTopics() {
    List<Topic> filteredTopics;
    if (_filter == true) {
      filteredTopics = _topics.toList();
      filteredTopics
          .sort((a, b) => b.stargazersCount.compareTo(a.stargazersCount));
    } else {
      filteredTopics = _topics.toList();
      filteredTopics.sort((a, b) => b.updatedAt.compareTo(a.updatedAt));
    }
    return filteredTopics;
  }

  List<Topic> get topics => state;
}

// -----------------------------------------------------------------------------

final filteredTopicsProvider =
    StateNotifierProvider<FilteredTopicsController, List<Topic>>((ref) {
  final topics = ref.watch(topicsProvider);
  final filter = ref.watch(topicSortProvider);
  return FilteredTopicsController(
    topics: topics,
    filter: filter,
  );
});
