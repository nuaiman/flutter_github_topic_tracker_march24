import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/topic_controller.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/topic_sort_controller.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../screens/topics.dart';

class InitializationController extends StateNotifier<bool> {
  final TopicsSortController _sort;
  final TopicsController _topics;
  InitializationController({
    required TopicsSortController sort,
    required TopicsController topics,
  })  : _sort = sort,
        _topics = topics,
        super(true);

  Future<void> initializeData(BuildContext context) async {
    final connectivity = await (Connectivity().checkConnectivity());
    await _sort.getFilter();
    if (connectivity == ConnectivityResult.none) {
      await _topics.loadFromSharedPreferences();
    } else {
      await _topics.fetchNextPage();
    }

    if (context.mounted) {
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (context) => const TopicsScreen(),
        ),
        (route) => false,
      );
    }
  }
}

// -----------------------------------------------------------------------------

final initializationProvider =
    StateNotifierProvider<InitializationController, bool>((ref) {
  final sort = ref.watch(topicSortProvider.notifier);
  final topics = ref.watch(topicsProvider.notifier);
  return InitializationController(
    sort: sort,
    topics: topics,
  );
});
