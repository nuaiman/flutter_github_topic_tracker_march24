import 'package:flutter/material.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/topic_sort_controller.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../widgets/topics_list.dart';

class TopicsScreen extends ConsumerWidget {
  const TopicsScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text('Flutter Topic Tracker'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Card(
              color: const Color(0xFF222228),
              surfaceTintColor: const Color(0xFF222228),
              child: IconButton(
                onPressed: () {
                  ref.read(topicSortProvider.notifier).changeFilter(context);
                },
                icon: const Icon(Icons.sort),
              ),
            ),
          ),
        ],
      ),
      body: const TopicsList(),
    );
  }
}
