import 'package:flutter/material.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/initilization_controller.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/widgets/circular_loader.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class InitializationScreen extends ConsumerStatefulWidget {
  const InitializationScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _InitializationViewState();
}

class _InitializationViewState extends ConsumerState<InitializationScreen> {
  @override
  void initState() {
    ref.read(initializationProvider.notifier).initializeData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularLoader(),
      ),
    );
  }
}
