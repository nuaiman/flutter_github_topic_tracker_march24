import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_github_topic_tracker_march24/models/topic.dart';

import '../../../../../utils/utils.dart';

class TopicDetails extends StatelessWidget {
  final Topic topic;
  const TopicDetails({super.key, required this.topic});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(topic.name),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: SizedBox(
                  width: width,
                  height: width,
                  child: CachedNetworkImage(
                    imageUrl: topic.ownerImageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Card(
                  color: const Color(0xFF222228),
                  surfaceTintColor: const Color(0xFF222228),
                  child: ListTile(
                    title: const Text(
                      'Owner\'s Name',
                      style: TextStyle(color: Colors.white),
                    ),
                    subtitle: Text(
                      topic.ownerName,
                      style: const TextStyle(color: Colors.white),
                    ),
                    trailing: Text(
                      formatDate(topic.updatedAt),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: const Text(
                  'Description',
                  style: TextStyle(color: Colors.white),
                ),
                subtitle: Text(
                  topic.description,
                  style: const TextStyle(color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
