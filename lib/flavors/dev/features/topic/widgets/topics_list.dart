import 'package:flutter/material.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/filtered_topics_controller.dart';
import 'package:flutter_github_topic_tracker_march24/flavors/dev/features/topic/controllers/topic_sort_controller.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../../models/topic.dart';
import '../controllers/topic_controller.dart';
import 'circular_loader.dart';
import 'topic_tile.dart';

class TopicsList extends ConsumerWidget {
  const TopicsList({
    super.key,
  });
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final topicsController = ref.read(topicsProvider.notifier);
    ref.watch(topicsProvider);
    ref.watch(topicSortProvider);
    final filteredTopics =
        ref.watch(filteredTopicsProvider.notifier).getFilteredTopics();

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: ListView.separated(
        itemCount: filteredTopics.length + 1,
        separatorBuilder: (context, index) => SizedBox(height: 10),
        itemBuilder: (context, index) {
          if (index < filteredTopics.length) {
            Topic topic = filteredTopics[index];
            return TopicTile(topic: topic, topicsController: topicsController);
          } else if (index == filteredTopics.length &&
              filteredTopics.isNotEmpty) {
            ref.read(topicsProvider.notifier).fetchNextPage();
            return const CircularLoader();
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
