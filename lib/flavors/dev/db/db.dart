import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../../models/topic.dart';

class TopicSharedPreferences {
  static const _key = 'topics';

  Future<void> saveTopics(List<Topic> topics) async {
    final prefs = await SharedPreferences.getInstance();
    final topicsJson = topics.map((topic) => topic.toJson()).toList();
    await prefs.setString(_key, jsonEncode(topicsJson));
  }

  Future<List<Topic>> getTopics() async {
    final prefs = await SharedPreferences.getInstance();
    final topicsJson = prefs.getString(_key);
    if (topicsJson != null) {
      final List<dynamic> decodedJson = jsonDecode(topicsJson);
      return decodedJson.map((json) => Topic.fromJson(json)).toList();
    }
    return [];
  }
}
