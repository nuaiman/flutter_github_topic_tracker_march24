import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String formatDate(DateTime dateTime) {
  return DateFormat('dd/MM/yy - hh:mm:ss').format(dateTime);
}

void showSnackbar(BuildContext context, String content) {
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Center(child: Text(content)),
    ),
  );
}
