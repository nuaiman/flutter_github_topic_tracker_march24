import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'flavors/dev/github_topics_app.dart';

void main() {
  runApp(
    const ProviderScope(child: GithubTopicsApp()),
  );
}
